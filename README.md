# Quickstart

## Install a package

### Requierments
 - The client directly call kubectl, you should have it working and connected to the kubernetes.

### kpm install
```bash
usage: kpm install group/package [--namespace NS]`
$ kpm install ant31/rocketchat --namespace rocketchat
```

#### select a version
`$ kpm install ant31/heapster --version 1.1.0`

#### Idempotent
Deployment try to be as much as possible idempotent. If the package/resources haven't changed it won't touch them.

to force a redeploy use --force
`$ kpm install ant31/rocketchat --force`



# Create a package
## kpm new
1. Create the package directory and manifest
```bash
usage: kpm new group/package [--with-comments]
kpm new ant31/dashboard
ls -R ant31/dashboard
```

2. Copy/Paste the Kubernetes yaml resources in ./templates
3. Edit the manifest.yaml

## kpm push
The package is ready to be pushed.
Package are pushed in private mode by default and it required an account:

1. For the first time use `kpm login`, if you don't have an account yet `kpm login --signup`

`kpm login [--signup]`

2. Push the package (from the package directory)
`kpm push [--force]`



## Compose an application stack
