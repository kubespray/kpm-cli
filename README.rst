===============================
kubespray package manager
===============================

.. image:: https://img.shields.io/pypi/v/kpm.svg
        :target: https://pypi.python.org/pypi/kpm

.. image:: https://img.shields.io/travis/ant31/kpm.svg
        :target: https://travis-ci.org/ant31/kpm

.. image:: https://readthedocs.org/projects/kpm/badge/?version=latest
        :target: https://readthedocs.org/projects/kpm/?badge=latest
        :alt: Documentation Status


Registry of kubernetes ready to use applications

* Documentation: https://kpm.readthedocs.org.

Features
--------

* TODO

Credits
---------

Antoine Legrand
