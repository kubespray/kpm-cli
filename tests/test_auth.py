#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_kpm_auth
----------------------------------

Tests for `kpm.auth` module.
"""

import unittest

import kpm.auth


class TestKpmAuth(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_token(self):
        pass

    def test_set_token(self):
        pass


if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
