.. highlight:: shell

============
Installation
============

At the command line::

    $ easy_install kpm

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv kpm
    $ pip install kpm
